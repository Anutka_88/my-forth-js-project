function getNum () {
let firstNum = prompt("Введіть перше число");
while (firstNum === "" || firstNum === null || isNaN(firstNum)) {
    firstNum = prompt ("Введіть перше число", firstNum);
}
firstNum = +firstNum;
let secondNum = prompt("Введіть друге число");
while (secondNum === "" || secondNum === null || isNaN(secondNum)) {
    secondNum = prompt ("Введіть друге число", secondNum);
}
secondNum = +secondNum;
let operation = prompt("Введіть математичну операцію, яку потрібно виконати: +, -, *, /");

switch (operation) {
    case "+":
        console.log(firstNum + secondNum);
        break;

    case "-":
        console.log(firstNum - secondNum);
        break;
    
    case "*":
        console.log(firstNum * secondNum);
        break;
    
    case "/":
        console.log(firstNum / secondNum);
        break;

    default:
        console.log("Невідома операція");

}

// function sumNum () {
//     return firstNum + secondNum;
// }
// function dobNum () {
//     return firstNum * secondNum;
// }
// function divNum () {
// return firstNum / secondNum;
// }
// function minusNum () {
//     return firstNum - secondNum;
// }
// if (operation === "*"){
// console.log(dobNum());
// }
// if (operation === "+"){
// console.log(sumNum());
//  }
//  if (operation === "-"){
// console.log(minusNum());
//  }
//  if (operation === "/"){
// console.log(divNum());
//  }
}

getNum();

